FROM amazonlinux:2

WORKDIR /tmp

#install the dependencies
RUN yum -y install gcc-c++ findutils aws-cli zip tar gzip jq git \
	yum clean all && \
    rm -rf /var/cache/yum

RUN touch ~/.bashrc && chmod +x ~/.bashrc

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash

RUN source ~/.bashrc && nvm install 14

WORKDIR /build