# Serving images with AWS Cloudfront and Lambda@Edge 

This repository provides all you need to get a fully working image service.

Original images will be stored in a S3 bucket, preferably in a region near your user.
They will be served by a Cloudfront distribution in the same region.

The distribution will be configured to use two Lambda@Edge function :
 
 - a `viewer-request` function that will rewrite the url with proper dimension and format
 - an `origin-response` function that will transform the orginal image to the dimension and format requested

Both function will be created in `us-east-1` region as Lambda@Edge is not supported in others regions.
A S3 bucket in the same region will be created to upload deployment package for theses functions.

## Gitlab pipeline

The pipeline contains 4 jobs.

 - The first job build a Docker Image based on nodeJS for the 3 others jobs
 - Second and third jobs build the deployment package for the 2 lambda functions. Nothing complicated, install dependencies, zip all things and send on S3
 - Last job is the deployment on AWS using Cloudformation

## Notes

 - The docker image is only build if Dockerfile change
 - The lamba functions are only build if their code change
 - The deployment package use versionning so if the code does not change, nothing will be deployed    

## Cost

Cloudfront cost depends on the data it send to the internet (first To is free), data downloaded from origin, requests count and function executed. Each month AWS offer:

 - 1 To of transfer out the internet
 - 10 millions requests

Lambda@Edge cost depends on function execution time (with a minimum of 50ms) and memory reserved (minimum 128M). Each function executed will cost 0,0000006 USD. 

 - The `viewer-request` reserve 128M of RAM and take between 2 and 5 ms 
 - The `origin-response` reserve 512M of RAM and can take up to 3000 ms when resizing.  

So for a website serving 10 millions images under 100Ko from 100 000 uniques images with a cache hit ratio of 90%. 

 - 9.13 USD for the `viewer-request` function 
 - 1.85 USD for the `origin-response` when resizing is already done but the image is not cached on Cloudfront
 - 7.56 USD for the `origin-response` that resizing every image once

So we have ~10 USD monthly for serving images and ~8 USD one time for resizing all images. CloudFront is free as we are in the free tier range.

The only thing missing is the cost of the S3 storage wich depends on your original image size. 